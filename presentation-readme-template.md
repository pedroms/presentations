# Presentation title

**👀 [View slides](presentation.pdf)**

**⬇️ [Download slides](https://gitlab.com/pedroms/presentations/raw/yyyy-mm-dd-event-name-presentation-title/presentation.pdf)**

## Summary

> Brief summary

## Details

- Event: [Event name](#)
- Date: MMM DD, YYYY
- Location: Venue, City, Country

## Thanks

🙌 to [First and Last Name](#)

Made using [Deckset](https://www.decksetapp.com)
