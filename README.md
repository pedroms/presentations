# Open source presentations

Whenever possible, I make the source content of my presentations available to
anyone. The presentations are written in
[Markdown](https://en.wikipedia.org/wiki/Markdown), a simple and future-proof
plain text format that can be read and changed by anyone. I use
[Deckset](https://www.decksetapp.com/) to render the Markdown content and
images into a visual format but any plain text editor can be used to extract
the contents.

Each presentation has its own branch in this repository, where you can find the
slides content and images.

1. [2019-11-16 @ GDG DevFest Coimbra 2019 - From closed to open design](https://gitlab.com/pedroms/presentations/tree/2019-11-16-gdg-coimbra-closed-open-design)
1. [2019-10-30 @ Zain Talks — Remote Design at GitLab](https://gitlab.com/pedroms/presentations/tree/2019-10-30-zain-talks-remote-design-gitlab)
1. [2019-03-27 @ Lisbon.UX — Remotely Open: Design Without Borders](https://gitlab.com/pedroms/presentations/tree/2019-03-27-lisbon-ux-remotely-open)
1. [2017-11-10 @ ergoUX — Design Aberto: Como a GitLab redesenhou a sua navegação (Portuguese)](https://gitlab.com/pedroms/presentations/tree/2017-11-10-ergoux-design-aberto)
1. [2017-05-10 @ DXD Talks — Remotely Open: Design Without Borders](https://gitlab.com/pedroms/presentations/tree/2017-05-10-dxd-talks-remotely-open)
